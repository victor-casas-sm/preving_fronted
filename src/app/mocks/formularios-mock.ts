
import {Formulario} from '../Model/Formulario/Formulario';

export const FORMULARIOSLIST: Formulario[] = [
  {
    id: '1',
    nombre: 'Formulario tipo 1'
  },
  {
    id: '2',
    nombre: 'Formulario tipo 2'
  },
  {
    id: '3',
    nombre: 'Formulario tipo 3'
  },
  {
    id: '4',
    nombre: 'Formulario tipo 4'
  },
  {
    id: '5',
    nombre: 'Formulario tipo 5'
  }
];



