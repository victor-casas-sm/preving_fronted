import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginAppComponent } from './login-app/login-app.component';
import { EngineComponent } from './engine/engine.component';
import { SidebarComponent } from './engine/sidebar/sidebar.component';
import { UserHeaderComponent } from './engine/user-header/user-header.component';
import { ModulesComponent } from './engine/modules/modules.component';
import { UserDocumentationComponent } from './engine/user-documentation/user-documentation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSelectModule} from '@angular/material/select';


import { BackofficeComponent } from './backoffice/backoffice.component';
import { MessageCenterComponent } from './engine/message-center/message-center.component';
import { PrevingFormsComponent } from './engine/preving-forms/preving-forms.component';
import { PrevingFaqComponent } from './engine/preving-faq/preving-faq.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    LoginAppComponent,
    EngineComponent,
    SidebarComponent,
    UserHeaderComponent,
    ModulesComponent,
    UserDocumentationComponent,
    BackofficeComponent,
    MessageCenterComponent,
    PrevingFormsComponent,
    PrevingFaqComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,


    MatSortModule,
    MatTableModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatSelectModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
