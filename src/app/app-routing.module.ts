import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EngineComponent } from 'src/app/engine/engine.component';
import { LoginComponent } from 'src/app/login/login.component';
import { LoginAppComponent } from 'src/app/login-app/login-app.component';
import { BackofficeComponent } from 'src/app/backoffice/backoffice.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'login-app', component: LoginAppComponent },
  { path: 'engine', component: EngineComponent },
  { path: 'backoffice', component: BackofficeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
