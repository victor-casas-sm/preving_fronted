import { Injectable } from '@angular/core';
import { User } from '../Model/User';
import { Table } from '../Model/Table/Table';
import {Centro} from '../Model/Centro';
import {Empresa} from '../Model/Empresa';
import {Formulario} from '../Model/Formulario/Formulario';

import { USER } from '../mocks/user-mock';
import { TABLE } from '../mocks/table-value-mock';
import {EMPRESAS} from '../mocks/empresas-mock';
import {CENTROSLIST} from '../mocks/centros-mock';
import {FORMULARIOSLIST} from '../mocks/formularios-mock';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor() { }
  /*get the full user data information*/
  getUser(): User {
    return USER;
  }

  /*get a table from a form*/
  getTable(): Table {
    return TABLE;
  }

   /*get a table from a form*/
   getCentros(): Centro[] {
    return CENTROSLIST;
  }

   /*get a table from a form*/
   getEmpresas(): Empresa[] {
    return EMPRESAS;
  }

   /*get a table from a form*/
   getFormulariosTypes(): Formulario[] {
    return FORMULARIOSLIST;
  }


}
