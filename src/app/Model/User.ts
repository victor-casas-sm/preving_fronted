import {Module} from './Module';

export interface User {
  id: number;//TO DO STRING
  name: string;
  surname: string;
  email: string;
  language: string;
  gestion: number;
  rol: number;
  modules: Module[];
}


