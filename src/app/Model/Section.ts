export interface Section {
  name: string;
  value: string;
  id: string;
}
