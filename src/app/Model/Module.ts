import {Section} from './Section';

export interface Module {
  id: string;
  name: string;
  section: Section[];
}


