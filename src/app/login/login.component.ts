import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../Model/User';

/*Routing */
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  imgUrl = '../assets/img/CrossBanner.png';
  logoUrl = '../assets/img/logo.svg';
  logoWhiteUrl = '../assets/img/logoPrevingWhite.svg';

  bannerObject = {
    title: 'CoronaVirus',
    detail: 'Con motivo del estado de alerta a nivel mundial sobre la epidemia del coronavirus 2019-nCoV, desde Grupo Preving queremos indicar algunas pautas claves del tema.',
    isButton: true,
    buttonDescription: 'SABER MÁS',
    imgUrl: '../assets/img/estrellas.jpg'
  };

  user: User;

  constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) {

  }

  ngOnInit(): void {

  }
  getUser(): void {
    this.user = this.userService.getUser();
  }

  goToEngine(): void {
    //if succes login//
    this.router.navigate(['backofficce']);
  }

}
