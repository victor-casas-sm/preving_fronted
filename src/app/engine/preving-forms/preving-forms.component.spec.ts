import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrevingFormsComponent } from './preving-forms.component';

describe('PrevingFormsComponent', () => {
  let component: PrevingFormsComponent;
  let fixture: ComponentFixture<PrevingFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrevingFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrevingFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
