import { Component, OnInit } from '@angular/core';

import { UserService } from '../../services/user.service';
import {SelectionModel} from '@angular/cdk/collections';
import {FormControl} from '@angular/forms';

import { Empresa } from '../../Model/Empresa';
import { Centro } from '../../Model/Centro';
import { Formulario } from '../../Model/Formulario/Formulario';

@Component({
  selector: 'app-preving-forms',
  templateUrl: './preving-forms.component.html',
  styleUrls: ['./preving-forms.component.css']
})
export class PrevingFormsComponent implements OnInit {

  empresasList: Empresa[];
  centrosList: Centro[];
  formularioType: Formulario[];

    /*selectors*/
    empresas = new FormControl();
    centros = new FormControl();
    formulariosTipo = new FormControl();


  constructor(private userService: UserService) {

  }

  ngOnInit(): void {
    this.centrosList = this.userService.getCentros();
    this.empresasList = this.userService.getEmpresas();
    this.formularioType = this.userService.getFormulariosTypes();
  }

}
