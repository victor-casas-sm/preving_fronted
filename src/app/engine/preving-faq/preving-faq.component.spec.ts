import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrevingFaqComponent } from './preving-faq.component';

describe('PrevingFaqComponent', () => {
  let component: PrevingFaqComponent;
  let fixture: ComponentFixture<PrevingFaqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrevingFaqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrevingFaqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
