import { Component, OnInit } from '@angular/core';

import { User } from '../Model/User';
import { UserService } from '../services/user.service';
import { Table } from '../Model/Table/Table';
import { Section } from '../Model/Section';

@Component({
  selector: 'app-engine',
  templateUrl: './engine.component.html',
  styleUrls: ['./engine.component.css']
})
export class EngineComponent implements OnInit {

  user: User;
  tablePreving: Table;
  sectionSelected: Section;

  constructor(private userService: UserService) {

  }

  ngOnInit(): void {
    this.getUser();
    this.getTable();
  }

  getUser(): void {
    this.user = this.userService.getUser();
  }

  getTable(): void {
    this.tablePreving = this.userService.getTable();
  }

  updateSection(section: Section): void {
    this.sectionSelected = section;
    console.log("Section recibida");
    console.log(this.sectionSelected);
  }

}
