import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Table } from '../../Model/Table/Table';
import {SelectionModel} from '@angular/cdk/collections';
import {FormControl} from '@angular/forms';


export interface PeriodicElement {
  [key: string]: any;
}


@Component({
  selector: 'app-user-documentation',
  templateUrl: './user-documentation.component.html',
  styleUrls: ['./user-documentation.component.css']
})
export class UserDocumentationComponent implements OnInit {
  /*selectors*/
  toppings = new FormControl();
  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];

  finalTableHeaders: string[] = [];
  finalTableDataSource = [];

  @Input() tablePreving: Table;

  dataSource = new MatTableDataSource();
  selection = new SelectionModel<PeriodicElement>(true, []);

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor() { }

  ngOnInit(): void {

    this.getTableHeaders();
    this.getTableDataSource();
    this.dataSource = new MatTableDataSource(this.finalTableDataSource);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }



  getTableHeaders(): void {
    this.tablePreving.tableHeader.forEach(element => {
      this.finalTableHeaders.push(element.fieldName);
    });
  }

  getTableDataSource(): void {
    this.tablePreving.tableInfo.forEach(row => {
      let aux = {};
      this.finalTableHeaders.forEach(headerFieldNames => {
        aux[headerFieldNames] = {};
      });
      //TO DO MEJORAR ESTE ALGORITMO!!!!!!!!!!!!!!!
      row.forEach(column => {
        this.finalTableHeaders.forEach(headerFieldNames => {
          if (headerFieldNames === column.fieldName) {
            aux[headerFieldNames] = column.cellValue;
          }
        });
      });
      this.finalTableDataSource.push(aux);
    });

  }

  //psudodinamy variables
  /*
  var variables = {};
var prefijo = 'texto';
for (var i = 0; i < 3; i++) {
  variables[prefijo + i] = 1;
  variables['nuevocampo']=1
}

for(var key in variables) {
  console.info(key);
}*/


//TO DO
/** Whether the number of selected elements matches the total number of rows. */
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataSource.data.length;
  return numSelected === numRows;
}

/** Selects all rows if they are not all selected; otherwise clear selection. */
masterToggle() {
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
}

/** The label for the checkbox on the passed row */
checkboxLabel(row?: PeriodicElement): string {
  if (!row) {
    return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
  }
  return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
}

}
