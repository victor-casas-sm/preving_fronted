import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../Model/User';
import { Section } from 'src/app/Model/Section';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input() userLoged: User;
  @Input() sectionSelected: Section;

  logoUrl = '../assets/img/logo.svg';


  bannerObject = {
    title: 'CoronaVirus',
    detail: 'Con motivo del estado de alerta a nivel mundial sobre la epidemia del coronavirus 2019-nCoV, desde Grupo Preving queremos indicar algunas pautas claves del tema.',
    isButton: true,
    buttonDescription: 'SABER MÁS',
    imgUrl: '../assets/img/estrellas.jpg'
  };

  constructor() { }

  ngOnInit(): void {

  }

}
