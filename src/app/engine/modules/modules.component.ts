import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/Model/User';
import { Section } from '../../Model/Section';
import { Module } from '../../Model/Module';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {

  @Input() userLoged: User;
  @Output() sectionActive = new EventEmitter<Section[]>();

  moduleSelected = {};


  modulesList = [
    /* { name: 'Preveción' },
     { name: 'Vigilancia' },
     { name: 'Gestión Administrativa' }*/
  ];

  constructor() {

  }

  ngOnInit(): void {
    this.modulesList = this.userLoged.modules;
  }


  selectModule(module : Module){
     this.sectionActive.emit(module.section);
  }


}
